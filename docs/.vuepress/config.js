const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: '  ',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    logo: '../small-logo.png',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    sidebarDepth: 3,
    nav: [
      {
        text: 'Home',
        link: '/index'
      },
      {
        text: 'User Guide',
        link: '/user-guide/'
      },
      {
        text: 'Developer Guide',
        link: '/developer-guide/'
      },
      {
        text: 'Project Status',
        link: '/status'
      },
      {
          text: 'Tutorial',
          link: '/tutorial/'
      }
    ],
    sidebar: {
      '/user-guide/': [
        {
          title: 'User Guide',
          collapsable: false,
          children: [
            '',
            'precept-script',
            'precept-schema',
            'libraries',
            'parts',
            'widget-tree',
            'installation',
          ]
        }
      ],
      '/developer-guide/': [
          {
              title: 'Developer',
              collapsable: false,
              children: [
                  '',
                  'backend-implementation',
                  'conventions',
                  'errors',
                  'kitchenSink',
              ]
          }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    'vuepress-plugin-mermaidjs',
    '@vuepress/blog',
  ]
}
