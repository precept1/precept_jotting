# Tutorial

A step by step guide to setting up a Flutter App using Precept.

## Assumptions

You are using Android Studio (with apologies to anyone using any other IDE).

## Conventions

- Restart the app :arrow_forward: (using either run or debug)
- Hot reload the app :zap:
- Check something has happened :white_check_mark:
- **import** statements may be shown with sample code. The IDE will usually find the imports, but not always.  Place imports at the top of the file

## Install the Package

// TODO 

## Create an App

- [Create the default Flutter Demo](https://flutter.dev/docs/get-started/test-drive#create-app) App.

- :arrow_forward: Run the app 

- :white_check_mark:  Make sure it works.



## Prepare for Precept

### Configure the Router

- Remove the following line:

```
home: MyHomePage(title: 'Flutter Demo Home Page'),
```
- Add these two lines to the MaterialApp configuration,

```
initialRoute: '/',
onGenerateRoute: router.generateRoute,
```

 :white_check_mark:`MyApp` should now look like this (with comments removed)

``` Dart
import 'package:precept_client/precept/router.dart';
```


```Dart {9,10}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      onGenerateRoute: router.generateRoute,
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
```


:arrow_forward: Run the app

### Define Precept Scripts

If you have read the [User Guide](../../user-guide/README.md), you will know that Precept uses JSON files to declare the User Interface and Schema.

Normally, Precept files are loaded from a server, but to keep things simple to being with, we are just going to load the configuration from a local declaration.

- create folder *lib/app/config*
- create file *lib/app/config/precept.dart*
- create file *lib/app/config/schema.dart*


These two files together form the configuration for a Precept App. [:point_right:](./user-guide)

- paste the following into *precept.dart*

```dart

import 'package:precept_client/precept/script/script.dart';
import 'package:precept_client/precept/part/string/stringPart.dart';

final myPrecept = PScript(name:'core',
      routes: [
        PRoute(
          path: '/',
          page: PPage(
              title: 'Home',
              document: PDocument(isStatic: true, sections: [
                PSection(
                    content: [PString(isStatic: true, static: 'Welcome to my Static home page')])
              ])),
        ),
      ],
);

```


### Load Your Scripts

We now have to tell Precept which scripts to use.  To do that, instantiate the appropriate implementation of `PreceptLoader`.

Usually scripts are loaded from a server, but for now we will just define them locally, and load them with an instance of `DirectPreceptLoader`.

- In *main.dart*, change the **main()** method to be:


``` dart
void main() async {
  await precept.init(
    loaders: [
      DirectPreceptLoader(script: myScript),
    ],
  );
  runApp(MyApp());
}

```

- :arrow_forward: Run the app 

- :white_check_mark:  Make sure it looks like this:

.


::: tip Dependency Injection

Precept uses [get_it](https://pub.dev/packages/get_it) for dependency injection.  If you are not familiar with it, it may be worth taking a look at their documentation but you do not need to do that now.

If you already know something like Guice or Dagger, the concepts are very similar.

::: 
