# Status

This is a broad statement of current project status, and will be updated every few days initially.

## Summary

Last updated: **02 Mar 2021**

- Decided that more needs to be done on Proof of Concept, before going public.  
- Validation completed, but then the structure (specifically the placement of the schema to facilitate permissions handling) has been changed. 




## Proof of Concept

Just need to complete the permissions handling, which may also have broken what was done for validation. 





